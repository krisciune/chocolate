import mediapark.chocolate.Charlie;
import mediapark.chocolate.ChocolateBar;

/**
 * Created by krutulin on 2016.06.01.
 */
public class MyCharlie extends Charlie {
    @Override
    protected boolean shouldEatChocolate(ChocolateBar chocolateBar) {
        return (chocolateBar.getSizeInKilograms() < 0.7
            && chocolateBar.getSugarContentInPercent() <= 5.0
            && this.getBloodSugarContentInKilograms() < 0.15);
    }

    @Override
    public ChocolateBar showChocolateBarToAugustus(ChocolateBar chocolateBar, boolean b) {
        if (!b) {
            return chocolateBar;
        } else {
            return new ChocolateWrapper(chocolateBar);
        }
    }
}
