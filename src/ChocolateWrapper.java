import mediapark.chocolate.ChocolateBar;

/**
 * Created by krutulin on 2016.06.01.
 */
public class ChocolateWrapper implements ChocolateBar {

    private ChocolateBar originalBar;

    public ChocolateWrapper(ChocolateBar originalBar) {
        this.originalBar = originalBar;
    }

    @Override
    public double getSugarContentInPercent() {
        return 0.0D;
    }

    @Override
    public double getSizeInKilograms() {
        return originalBar.getSizeInKilograms();
    }

    @Override
    public void takeABite(double v) {
        originalBar.takeABite(v);
    }
}
