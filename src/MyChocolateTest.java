import mediapark.chocolate.Charlie;
import mediapark.chocolate.ChocolateFactory;
import mediapark.test.AbstractChocolateTest;

/**
 * Created by krutulin on 2016.06.01.
 */
public class MyChocolateTest extends AbstractChocolateTest {

    @Override
    protected ChocolateFactory getChocolateFactory() {
        return new MyChocolateFactory();
    }

    @Override
    protected Charlie getCharlie() {
        return new MyCharlie();
    }
}
