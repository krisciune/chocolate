import mediapark.chocolate.ChocolateFactory;
import mediapark.chocolate.DefaultChocolateBar;
import mediapark.chocolate.NoMoreCocoaException;

/**
 * Created by krutulin on 2016.06.01.
 */
public class MyChocolateFactory implements ChocolateFactory {

    private double cocoa = 1.025D;

    @Override
    public void receiveCocoaShippment(double v) {
        cocoa += v;
    }

    @Override
    public DefaultChocolateBar produceNextChocolateBar() throws NoMoreCocoaException {
        if (cocoa < 0.025D) {
            throw new NoMoreCocoaException("no more cocoa");
        }
        cocoa -= 0.025D;
        return new DefaultChocolateBar(0.5D, 4.6D);
    }
}
